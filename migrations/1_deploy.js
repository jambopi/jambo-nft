const Web3 = require("web3");
const chalk = require("cli-color");
const ContractDeployerWithTruffle = require("@evmchain/contract-deployer/src/truffle");
const { networks } = require("../truffle-config.js");

module.exports = async function (deployer, network, accounts) {
  const { provider } = networks[network] || {};
  if (!provider) {
    throw new Error(`Unable to find provider for network: ${network}`);
  }

  const web3 = new Web3(provider);
  const deployConfig = {
    dataFilename: `./networks/${network}.json`,
    deployData: require(`../networks/${network}.json`),
    proxyAdminName: "JamboProxyAdmin",
    proxyName: "JamboProxy",
  };

  const contractDeployer = new ContractDeployerWithTruffle({
    artifacts,
    deployer,
  });
  contractDeployer.setWeb3(web3);
  contractDeployer.setConfig(deployConfig);

  // Initialize
  await contractDeployer.init();

  // Deploy contract
  await contractDeployer.deployAllManifests({
    args: {
      JamboNFT: {
        initArgs: ["config:jambonft.name", "config:jambonft.symbol", "config:jambonft.baseURI"],
      },
    },
  });

  // Grant roles
  await contractDeployer.grantRoles();

  // await mint(contractDeployer);
  // await mintForProd(contractDeployer);
};

const mint = async (contractDeployer) => {
  console.log(`\n\nMinting NFTs...`);

  const tokens = [
    {
      nft: "A",
      to: "0x070Bf7B969794b9080949Fd41AB8e3E2c8480c8D",
    },
    {
      nft: "B",
      to: "0x4E343C75b8BF02B4eC649F925Fae82FdB96524B0",
    },
    {
      nft: "C",
      to: "0x6786984B4137281d3Bc9586399E51FaFd8CD9e24",
    },
    {
      nft: "D",
      to: "0x314Ea8962052f0E82868A673D0A2C183b1F36e39",
    },
    {
      nft: "E",
      to: "0x36A383a7dC735fDA7BA50321bD505dEC4732CfF4",
    },
    {
      nft: "F",
      to: "0x7858806525EBA66868778B22712cD222Da3D7cEd",
    },
    {
      nft: "G",
      to: "", // Update me
    },
    {
      nft: "H",
      to: "", // Update me
    },
  ];

  let tokenMaster = await contractDeployer.loadContract("TokenMaster");
  for (let i = 0; i < tokens.length; i = i + 1) {
    const contractAddress = contractDeployer.formatValue(
      `address:JamboERC721${tokens[i].nft}`
    );
    tx = await tokenMaster.mintItem(contractAddress, tokens[i].to);
    console.log(`\tMINTED ${tokens[i].nft} ${contractAddress} tx:${tx.tx}`);
  }

  console.log(`\n`);
};

const mintForProd = async (contractDeployer) => {
  console.log(`\n\nMinting NFTs...`);

  const tokens = [
    {
      nft: "A",
      to: contractDeployer.formatValue("config:admin.address"),
    },
    // {
    //   nft: "B",
    //   to: "0x4E343C75b8BF02B4eC649F925Fae82FdB96524B0",
    // },
    // {
    //   nft: "C",
    //   to: "0x6786984B4137281d3Bc9586399E51FaFd8CD9e24",
    // },
    // {
    //   nft: "D",
    //   to: "0x314Ea8962052f0E82868A673D0A2C183b1F36e39",
    // },
    // {
    //   nft: "E",
    //   to: "0x36A383a7dC735fDA7BA50321bD505dEC4732CfF4",
    // },
    // {
    //   nft: "F",
    //   to: "0x7858806525EBA66868778B22712cD222Da3D7cEd",
    // },
    // {
    //   nft: "G",
    //   to: "", // Update me
    // },
    // {
    //   nft: "H",
    //   to: "", // Update me
    // },
  ];

  let tokenMaster = await contractDeployer.loadContract("TokenMaster");
  for (let i = 0; i < tokens.length; i = i + 1) {
    const contractAddress = contractDeployer.formatValue(
      `address:JamboERC721${tokens[i].nft}`
    );
    tx = await tokenMaster.mintItem(contractAddress, tokens[i].to);
    console.log(`\tMINTED ${tokens[i].nft} ${contractAddress} tx:${tx.tx}`);
  }

  console.log(`\n`);
};