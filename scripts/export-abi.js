const fs = require('fs');

const ABI_DIR = './abis';

function exportAbi(name) {
  console.log('- Export Abi for contract: ', name);
  let buildJson = require(`../build/contracts/${name}`);
  let exportAbi = {
    contractName: buildJson.contractName,
    abi: buildJson.abi
  }

  fs.writeFileSync(`${ABI_DIR}/${name}.json`, JSON.stringify(exportAbi, null, 2));
}

(async () => {

  console.log('* Export ABIs');

  const contractNames = [
    'JamboNFT',
  ];
  for (let name of contractNames) {
    exportAbi(name);
  }
})();