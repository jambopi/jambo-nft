// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;

import "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/ERC721URIStorageUpgradeable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "./abstract/AbstractRole.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";

contract JamboNFT is ERC721URIStorageUpgradeable, AbstractRole {
  using Counters for Counters.Counter;

  event NFTMinted(uint256 indexed tokenId, address to, string tokenURI);

  Counters.Counter private _tokenIdTracker;

  string private _baseTokenURI;

  bool public transferDisabled;

  modifier onlyOperator() {
    require(hasRole(OPERATOR_ROLE, msg.sender), "JAMBONFT_CALLER_IS_NOT_ADMIN");
    _;
  }

  function initialize(
    string memory name_,
    string memory symbol_,
    string memory baseTokenURI_
  ) public initializer {
    __AccessControl_init();
    __ERC721_init(name_, symbol_);

    _baseTokenURI = baseTokenURI_;
    _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
    _grantRole(OPERATOR_ROLE, msg.sender);
  }

  function _baseURI() internal view virtual override returns (string memory) {
    return _baseTokenURI;
  }

  function setBaseURI(string memory baseTokenURI_) external onlyOperator {
    _baseTokenURI = baseTokenURI_;
  }

  function _mint(address to) internal virtual returns (uint256) {
    _tokenIdTracker.increment();
    uint256 tokenId = _tokenIdTracker.current();
    _safeMint(to, tokenId);
    return tokenId;
  }

  function mint(address to_, string memory tokenURI_) external onlyOperator {
    uint256 tokenId = _mint(to_);
    _setTokenURI(tokenId, tokenURI_);

    emit NFTMinted(tokenId, to_, tokenURI_);
  }

  function mintBatch(
    address[] memory recipients_,
    string[] memory tokenURIs_
  ) public {
    require(
      recipients_.length == tokenURIs_.length,
      "JAMBONFT_INPUT_IS_INVALID"
    );
    for (uint256 i = 0; i < recipients_.length; i++) {
      uint256 tokenId = _mint(recipients_[i]);
      _setTokenURI(tokenId, tokenURIs_[i]);
    }
  }

  function disperse(
    address[] memory recipients_,
    uint256[] memory tokenIds_
  ) external {
    require(
      recipients_.length == tokenIds_.length,
      "JAMBONFT_INPUT_IS_INVALID"
    );

    for (uint256 i = 0; i < recipients_.length; i++)
      _safeTransfer(msg.sender, recipients_[i], tokenIds_[i], "");
  }

  function burn(uint256 tokenId_) external onlyOperator {
    _burn(tokenId_);
  }

  function supportsInterface(
    bytes4 interfaceId
  )
    public
    view
    override(ERC721URIStorageUpgradeable, AccessControlUpgradeable)
    returns (bool)
  {
    return super.supportsInterface(interfaceId);
  }
}
