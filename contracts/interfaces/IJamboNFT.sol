// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;

interface IJamboNFT {
  function mint(address to_) external returns (uint256);

  function burn(uint256 tokenId_) external;
}
