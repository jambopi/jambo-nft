require("dotenv").config();

const HDWalletProvider = require("@truffle/hdwallet-provider");

const WALLET_TESTNET = process.env.DEPLOY_PRIVATE_KEY_TESTNET;
const WALLET_MAINNET = process.env.DEPLOY_PRIVATE_KEY_MAINNET;

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 9545,
      network_id: "*",
    },
    "goerli-dev": {
      provider: new HDWalletProvider(
        WALLET_TESTNET,
        `https://goerli.infura.io/v3/d2741a9839d24b12a40bf00170c1c9b6`,
        0,
        1,
        true,
        "m/44'/60'/0'/0/",
        5
      ),
      network_id: "*",
      timeoutBlocks: 40000,
      confirmations: 1,
      skipDryRun: true,
    },
    "bsc-dev": {
      provider: new HDWalletProvider(
        WALLET_TESTNET,
        `https://data-seed-prebsc-1-s1.binance.org:8545/`,
        0,
        1,
        true,
        "m/44'/60'/0'/0/",
        97
      ),
      network_id: "*",
      timeoutBlocks: 40000,
      confirmations: 1,
      skipDryRun: true,
    },
  },

  mocha: {
    timeout: 100000,
  },

  compilers: {
    solc: {
      version: "0.8.10",
      settings: {
        optimizer: {
          enabled: true,
          runs: 200,
        },
      },
    },
  },

  db: {
    enabled: false,
  },
};
